package com.tessafold.hellofolk.ui.post_details

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.runner.lifecycle.ActivityLifecycleCallback
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import com.tessafold.hellofolk.R
import com.tessafold.hellofolk.data.network.Resource
import com.tessafold.utils.generatePosts
import io.mockk.every
import io.mockk.mockk
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class PostDetailsActivityTest {

    private val mockViewModel = mockk<PostDetailsViewModel>()

    lateinit var scenario: ActivityScenario<PostDetailsActivity>

    private val post = generatePosts()[0]

    private fun launchActivity(): ActivityScenario<PostDetailsActivity> {
        val intent = Intent(
            ApplicationProvider.getApplicationContext(),
            PostDetailsActivity::class.java
        ).putExtra(POST_INTENT_KEY, post)
        return ActivityScenario.launch(intent)
    }

    private val activityLifeCycleObserver = ActivityLifecycleCallback { activity, stage ->
        when (stage) {
            Stage.PRE_ON_CREATE -> {
                if (activity is PostDetailsActivity) {
                    activity.viewModel = mockViewModel
                }
            }
            else -> {
                // nothing
            }
        }
    }

    @Before
    fun setup() {
        every { mockViewModel.getComments(any()) } answers { nothing }
        every { mockViewModel.commentsLiveData } returns  MutableLiveData()

        ActivityLifecycleMonitorRegistry.getInstance()
            .addLifecycleCallback(activityLifeCycleObserver)
    }

    @After
    fun teardown() {
        ActivityLifecycleMonitorRegistry.getInstance()
            .removeLifecycleCallback(activityLifeCycleObserver)
        scenario.close()
    }

    @Test
    fun shouldLaunchWithoutCrash() {
        scenario = launchActivity()
    }

    @Test
    fun shouldDisplayPostDetails() {
        scenario = launchActivity()

        onView(withId(R.id.username)).check(matches(withText(post.user?.name)))
        onView(withId(R.id.title)).check(matches(withText(post.title)))
        onView(withId(R.id.body)).check(matches(withText(post.body)))
    }

    @Test
    fun shouldDisplayProgressAnimationWhenCommentsAreBeingLoaded() {
        // Arrange
        every { mockViewModel.commentsLiveData } returns MutableLiveData(Resource.loading())

        // Act
        scenario = launchActivity()

        // Assert
        onView(withId(R.id.commentsRecyclerView)).check(matches(not(isDisplayed())))
        onView(withId(R.id.errorTextView)).check(matches(not(isDisplayed())))
        onView(withId(R.id.animationView)).check(matches(isDisplayed()))
    }


    @Test
    fun shouldDisplayErrorMessageWhenCommentFetchFails(){
        // Arrange
        val message = "Unknown error has occurred!"
        every { mockViewModel.commentsLiveData } returns MutableLiveData(Resource.error(message))

        // Act
        scenario = launchActivity()

        // Assert
        onView(withId(R.id.commentsRecyclerView)).check(matches(not(isDisplayed())))
        onView(withId(R.id.animationView)).check(matches(not(isDisplayed())))
        onView(withId(R.id.errorTextView)).check(matches(isDisplayed()))
        onView(withId(R.id.errorTextView)).check(matches(withText(message)))
    }

    /**
     *
     * AND SO ON!
     *
     */

}