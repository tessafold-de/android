package com.tessafold.hellofolk.ui.posts

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.runner.lifecycle.ActivityLifecycleCallback
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import com.tessafold.hellofolk.R
import com.tessafold.hellofolk.data.model.Post
import com.tessafold.hellofolk.data.network.Resource
import com.tessafold.utils.RecyclerViewMatcher
import com.tessafold.utils.generatePosts
import io.mockk.every
import io.mockk.mockk
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PostsActivityTest {

    private val mockViewModel: PostsViewModel = mockk()

    lateinit var scenario: ActivityScenario<PostsActivity>

    private fun launchActivity() = ActivityScenario.launch(PostsActivity::class.java)


    private val activityLifeCycleObserver = ActivityLifecycleCallback { activity, stage ->
        when (stage) {
            Stage.PRE_ON_CREATE -> {
                if (activity is PostsActivity) {
                    activity.viewModel = mockViewModel
                }
            }
            else -> {
                // nothing
            }
        }
    }

    @Before
    fun setup() {
        every { mockViewModel.fetchData() } answers { nothing }
        every { mockViewModel.isRefreshing } answers { MutableLiveData(false) }
        ActivityLifecycleMonitorRegistry.getInstance()
            .addLifecycleCallback(activityLifeCycleObserver)
    }

    @After
    fun tearDown() {
        ActivityLifecycleMonitorRegistry.getInstance()
            .removeLifecycleCallback(activityLifeCycleObserver)
        scenario.close()
    }

    @Test
    fun shouldLaunchWithoutCrash() {
        // Arrange
        every { mockViewModel.mediatorLiveData } returns MediatorLiveData()

        // Act
        scenario = launchActivity()

        // Assert
        // -> The test should pass when no exception is thrown!
    }

    @Test
    fun whenStatusIsLoadingShouldDisplayLoadingAnimation() {
        // Arrange
        val mediatorLiveData = MediatorLiveData<Resource<List<Post>>>()
        mediatorLiveData.addSource(MutableLiveData(Resource.loading(data = null))) {
            mediatorLiveData.value = it
        }
        every { mockViewModel.mediatorLiveData } returns mediatorLiveData

        // Act
        scenario = launchActivity()

        // Assert
        onView(withId(R.id.animationView)).check(matches(isDisplayed()))
        onView(withId(R.id.postsRecyclerView)).check(matches(not(isDisplayed())))
        onView(withId(R.id.errorTextView)).check(matches(not(isDisplayed())))
    }

    @Test
    fun whenStatusIsErrorShouldDisplayErrorTextView() {
        // Arrange
        val message = "Something went wrong :("
        val mediatorLiveData = MediatorLiveData<Resource<List<Post>>>()
        mediatorLiveData.addSource(MutableLiveData(Resource.error(message, data = null))) {
            mediatorLiveData.value = it
        }
        every { mockViewModel.mediatorLiveData } returns mediatorLiveData
        // Act
        scenario = launchActivity()

        // Assert
        onView(withId(R.id.errorTextView)).check(matches(isDisplayed()))
        onView(withId(R.id.errorTextView)).check(matches(withText(message)))
        onView(withId(R.id.animationView)).check(matches(not(isDisplayed())))
        onView(withId(R.id.postsRecyclerView)).check(matches(not(isDisplayed())))
    }

    @Test
    fun whenStatusIsLoadedRecyclerViewWithTheDataIsDisplayed() {
        // Arrange
        val posts = generatePosts()
        val mediatorLiveData = MediatorLiveData<Resource<List<Post>>>()
        mediatorLiveData.addSource(MutableLiveData(Resource.success(posts))) {
            mediatorLiveData.value = it
        }
        every { mockViewModel.mediatorLiveData } returns mediatorLiveData

        // Act
        scenario = launchActivity()

        // Assert
        onView(withId(R.id.animationView)).check(matches(not(isDisplayed())))
        onView(withId(R.id.errorTextView)).check(matches(not(isDisplayed())))
        onView(withId(R.id.postsRecyclerView)).check(matches(isDisplayed()))

        // Items should be displayed correctly
        posts.forEachIndexed { index, post ->
            onView(
                RecyclerViewMatcher(R.id.postsRecyclerView)
                    .atPositionOnView(index, R.id.username)
            )
                .check(matches(withText(post.user?.name)))

            onView(
                RecyclerViewMatcher(R.id.postsRecyclerView)
                    .atPositionOnView(index, R.id.title)
            )
                .check(matches(withText(post.title)))

            onView(
                RecyclerViewMatcher(R.id.postsRecyclerView)
                    .atPositionOnView(index, R.id.body)
            )
                .check(matches(withText(post.body)))
        }
    }


}