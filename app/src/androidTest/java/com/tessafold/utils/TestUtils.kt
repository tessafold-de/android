package com.tessafold.utils

import com.tessafold.hellofolk.data.model.Post
import com.tessafold.hellofolk.data.model.User


fun generatePosts(): List<Post> {
    val result: MutableList<Post> = mutableListOf()
    var post: Post?

    post = Post(id = 1, userId = 1, title = "Post Title", body = "Post Body")
    post.user = User(
        id = 1,
        name = "Hamza Al Omari",
        email = "omarihamza@outlook.com",
        phone = "+41132456789"
    )

    result += post

    post = Post(id = 2, userId = 2, title = "Post Title 2", body = "Post Body 2")

    post.user = User(
        id = 2,
        name = "User",
        email = "user@mail.de",
        phone = "+41132456789"
    )

    result += post

    return result
}

