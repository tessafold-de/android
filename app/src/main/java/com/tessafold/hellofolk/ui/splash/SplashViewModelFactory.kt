package com.tessafold.hellofolk.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tessafold.hellofolk.data.prefs.AppPreferences

@Suppress("UNCHECKED_CAST")
class SplashViewModelFactory(private val appPreferences: AppPreferences) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SplashViewModel(appPreferences = appPreferences) as T
    }
}