package com.tessafold.hellofolk.ui.intro

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import com.tessafold.hellofolk.R
import com.tessafold.hellofolk.databinding.IntroPagerItemBinding

data class PagerItem(
    val jsonResourceId: Int,
    val title: String,
    val body: String
)

class MyViewPagerAdapter(private val items: List<PagerItem>) : PagerAdapter() {
    override fun getCount(): Int = items.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layoutInflater =
            container.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding = DataBindingUtil.inflate<IntroPagerItemBinding>(
            layoutInflater,
            R.layout.intro_pager_item,
            container,
            false
        )
        binding.animationView.setAnimation(items[position].jsonResourceId)
        binding.animationView.playAnimation()
        binding.item = items[position]
        container.addView(binding.root)
        return binding.root
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

}