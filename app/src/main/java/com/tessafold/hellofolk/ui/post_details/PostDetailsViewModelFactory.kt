package com.tessafold.hellofolk.ui.post_details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tessafold.hellofolk.data.repository.CommentsRepository

@Suppress("UNCHECKED_CAST")
class PostDetailsViewModelFactory(
    private val commentsRepository: CommentsRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostDetailsViewModel(commentsRepository) as T
    }
}