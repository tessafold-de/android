package com.tessafold.hellofolk.ui.post_details

import com.tessafold.hellofolk.R
import com.tessafold.hellofolk.data.model.Comment
import com.tessafold.hellofolk.ui.base.BaseAdapter

class CommentsAdapter(comments: List<Comment>) : BaseAdapter<Comment>(comments) {

    override fun getLayoutIdForPosition(position: Int): Int = R.layout.viewholder_comment_item

    override fun onItemClicked(item: Comment) {
    }


}