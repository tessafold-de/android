package com.tessafold.hellofolk.ui.posts

import com.tessafold.hellofolk.R
import com.tessafold.hellofolk.data.model.Post
import com.tessafold.hellofolk.data.model.User
import com.tessafold.hellofolk.generated.callback.OnClickListener
import com.tessafold.hellofolk.ui.base.BaseAdapter

class PostsAdapter(
    posts: List<Post>,
    private val postsListener: PostsListener
) : BaseAdapter<Post>(posts) {

    override fun getLayoutIdForPosition(position: Int): Int = R.layout.viewholder_post


    override fun onItemClicked(item: Post) {
        postsListener.onItemClicked(item)
    }


}