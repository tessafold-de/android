package com.tessafold.hellofolk.ui.intro

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tessafold.hellofolk.data.prefs.AppPreferences

@Suppress("UNCHECKED_CAST")
class IntroViewModelFactory(private val appPreferences: AppPreferences) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return IntroViewModel(appPreferences) as T
    }
}