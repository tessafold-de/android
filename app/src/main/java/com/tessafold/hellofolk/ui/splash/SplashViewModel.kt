package com.tessafold.hellofolk.ui.splash

import androidx.lifecycle.ViewModel
import com.tessafold.hellofolk.data.prefs.AppPreferences

class SplashViewModel(private val appPreferences: AppPreferences) : ViewModel() {

    fun isFirstOpen() = appPreferences.isFirstOpen()

}