package com.tessafold.hellofolk.ui.posts

import com.tessafold.hellofolk.data.model.Post

interface PostsListener {

    fun onItemClicked(post: Post)

}