package com.tessafold.hellofolk.ui.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tessafold.hellofolk.BR
import com.tessafold.hellofolk.MyApplication
import org.kodein.di.DI
import org.kodein.di.DIAware

abstract class BaseActivity<V : ViewModel, B : ViewDataBinding> : AppCompatActivity(), DIAware {

    override lateinit var di: DI

    abstract val layoutId: Int

    // This value needs to be mutable so that we can mock it in the tests
    abstract val viewModelFactory: ViewModelProvider.Factory

    abstract val viewModelClass: Class<V>

    @SuppressWarnings
    lateinit var viewModel: V

    lateinit var binding: B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        di = (applicationContext as MyApplication).di
        binding = DataBindingUtil.setContentView(this, layoutId)
        viewModel = if (this::viewModel.isInitialized) viewModel else ViewModelProvider(
            this,
            viewModelFactory
        ).get(viewModelClass)
        binding.lifecycleOwner = this
        binding.setVariable(BR.viewModel, viewModel)
        initUi()
        readIntent()
        getData()
    }


    abstract fun initUi()

    abstract fun readIntent()

    abstract fun getData()

}