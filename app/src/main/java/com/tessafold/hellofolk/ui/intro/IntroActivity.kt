package com.tessafold.hellofolk.ui.intro

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.tessafold.hellofolk.R
import com.tessafold.hellofolk.databinding.ActivityIntroBinding
import com.tessafold.hellofolk.ui.base.BaseActivity
import com.tessafold.hellofolk.ui.posts.PostsActivity
import kotlinx.android.synthetic.main.activity_intro.*
import org.kodein.di.instance

class IntroActivity : BaseActivity<IntroViewModel, ActivityIntroBinding>(),
    ViewPager.OnPageChangeListener, IntroListeners {

    override val layoutId: Int = R.layout.activity_intro

    override val viewModelFactory: ViewModelProvider.Factory by instance<IntroViewModelFactory>()
    override val viewModelClass: Class<IntroViewModel> = IntroViewModel::class.java

    override fun initUi() {
        // Init the ViewPager
        val pagerItems = listOf(
            PagerItem(
                jsonResourceId = R.raw.first_slide_animation,
                title = getString(R.string.pager_item_title),
                body = getString(R.string.pager_item_body)
            ),
            PagerItem(
                jsonResourceId = R.raw.second_slide_animation,
                title = getString(R.string.pager_item_title),
                body = getString(R.string.pager_item_body)
            )
        )
        val adapter = MyViewPagerAdapter(pagerItems)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(this)

        // Set ViewModel Listeners
        viewModel.introListeners = this

    }

    override fun readIntent() {
    }

    override fun getData() {
    }

    /**
     * We need to know the current page in order to display/hide pager next and previous controls
     * The values gets updated in the xml binding and the view will act accordingly
     */
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        viewModel.currentIndex.postValue(position)
        println(position)
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onNextPage() {
        if (viewPager.currentItem > 0) {
            // Change the value in SharedPreferences in order to disable the intro in future opens
            viewModel.setNotFirstOpen()

            // Launch PostsActivity
            val intent = Intent(this, PostsActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            // Scroll to next item
            viewPager.currentItem = viewPager.currentItem + 1
        }
    }

    override fun onPreviousPage() {
        viewPager.currentItem -= 1
    }

}