package com.tessafold.hellofolk.ui.post_details

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.tessafold.hellofolk.R
import com.tessafold.hellofolk.data.model.Post
import com.tessafold.hellofolk.data.network.Resource
import com.tessafold.hellofolk.databinding.ActivityPostDetailsBinding
import com.tessafold.hellofolk.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_post_details.*
import kotlinx.android.synthetic.main.activity_posts.*
import kotlinx.android.synthetic.main.activity_posts.mToolbar
import org.kodein.di.instance
import retrofit2.http.POST

const val POST_INTENT_KEY = "POST"

class PostDetailsActivity : BaseActivity<PostDetailsViewModel, ActivityPostDetailsBinding>() {

    lateinit var post: Post

    companion object {
        fun getIntent(context: Context, post: Post): Intent {
            val intent = Intent(context, PostDetailsActivity::class.java)
            intent.putExtra(POST_INTENT_KEY, post)
            return intent
        }
    }

    override val layoutId: Int = R.layout.activity_post_details

    override val viewModelFactory: ViewModelProvider.Factory by instance<PostDetailsViewModelFactory>()

    override val viewModelClass: Class<PostDetailsViewModel> = PostDetailsViewModel::class.java

    override fun initUi() {
        mToolbar.setNavigationOnClickListener { finish() }
    }

    override fun readIntent() {
        post = intent.getSerializableExtra(POST_INTENT_KEY) as Post
        binding.item = post
    }

    override fun getData() {
        viewModel.getComments(post.id)
        viewModel.commentsLiveData.observe(this, Observer {
            when (it.status) {
                Resource.Status.ERROR -> {
                }

                Resource.Status.LOADING -> {

                }

                Resource.Status.SUCCESS -> {
                    commentsRecyclerView.adapter = CommentsAdapter(it.data ?: emptyList())
                }
            }
        })
    }

}