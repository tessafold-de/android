package com.tessafold.hellofolk.ui.posts

import androidx.lifecycle.*
import com.tessafold.hellofolk.data.model.Post
import com.tessafold.hellofolk.data.model.User
import com.tessafold.hellofolk.data.network.Resource
import com.tessafold.hellofolk.data.repository.PostsRepository
import com.tessafold.hellofolk.data.repository.UsersRepository

open class PostsViewModel(
    private val postsRepository: PostsRepository,
    private val usersRepository: UsersRepository
) : ViewModel() {

    /**
     * We need to chain 2 API requests in order to fetch the author (aka. user) of the post
     * That's why we need to user a mediator live data
     * 1 -> we fetch the posts
     * 2 -> when the posts are fetched successfully we need to fetch the users and map them to the posts
     *
     * This process would have been easier with a proper api response :(
     *
     */

    val mediatorLiveData = MediatorLiveData<Resource<List<Post>>>()


    /**
     * This value is used by the SwipeRefreshLayout
     * The progress will be displayed according to its value
     * Its value is set to:
     *      -> true in refresh() method
     *      -> false when all data is fetched
     */
    val isRefreshing: MutableLiveData<Boolean> = MutableLiveData(false)


    /**
     * 1 -> Fetch Posts
     */
    fun fetchData() {
        mediatorLiveData.addSource(
            postsRepository.getPosts().asLiveData(viewModelScope.coroutineContext)
        ) {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    val data: List<Post> = it.data ?: emptyList()
                    fetchUsers(data)
                }
                else -> {
                    mediatorLiveData.value = it
                    isRefreshing.postValue(false)
                }
            }
        }


    }

    /**
     * 2 -> Fetch users & map them
     */
    private fun fetchUsers(posts: List<Post>) = mediatorLiveData.addSource(
        usersRepository.getUsers().asLiveData(viewModelScope.coroutineContext)
    ) {
        when (it.status) {
            Resource.Status.SUCCESS -> {
                val users: List<User> = it.data ?: emptyList()
                mediatorLiveData.value = Resource.success(posts.map { post ->
                    post.apply {
                        user = users.first { user -> user.id == post.userId }
                    }
                })
            }
            Resource.Status.LOADING -> {
                mediatorLiveData.value = Resource.loading(data = posts)
            }
            Resource.Status.ERROR -> {
                mediatorLiveData.value = Resource.error(it.message!!)
            }
        }
        isRefreshing.postValue(false)
    }

    fun refresh() {
        isRefreshing.postValue(true)
        fetchData()
    }

}