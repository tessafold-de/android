package com.tessafold.hellofolk.ui.intro

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tessafold.hellofolk.data.prefs.AppPreferences

class IntroViewModel(private val appPreferences: AppPreferences) : ViewModel() {

    val currentIndex = MutableLiveData(0)

    lateinit var introListeners: IntroListeners

    fun nextPage() = introListeners.onNextPage()

    fun previousPage() = introListeners.onPreviousPage()

    fun setNotFirstOpen() = appPreferences.setFirstOpen(false)

}