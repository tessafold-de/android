package com.tessafold.hellofolk.ui.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.RecyclerView
import com.tessafold.hellofolk.BR

abstract class BaseAdapter<T>(private val items: List<T>) :
    RecyclerView.Adapter<BaseAdapter.MyViewHolder<T>>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder<T> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, viewType, parent, false)
        return MyViewHolder(binding = binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder<T>, position: Int) {
        holder.itemView.setOnClickListener { onItemClicked(items[position]) }
        holder.bind(getItemForPosition(position))
    }

    override fun getItemViewType(position: Int): Int = getLayoutIdForPosition(position)

    private fun getItemForPosition(position: Int): T = items[position]

    override fun getItemCount(): Int = items.size

    protected abstract fun getLayoutIdForPosition(position: Int): Int

    protected abstract fun onItemClicked(item: T)

    class MyViewHolder<T>(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: T) {
            binding.setVariable(BR.item, item)
            binding.executePendingBindings()
        }
    }
}

