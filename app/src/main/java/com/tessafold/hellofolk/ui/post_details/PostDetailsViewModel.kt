package com.tessafold.hellofolk.ui.post_details

import androidx.lifecycle.*
import com.tessafold.hellofolk.data.model.Comment
import com.tessafold.hellofolk.data.network.Resource
import com.tessafold.hellofolk.data.repository.CommentsRepository

class PostDetailsViewModel(
    private val commentsRepository: CommentsRepository
) : ViewModel() {

    var commentsLiveData: LiveData<Resource<List<Comment>>> = MutableLiveData()

    fun getComments(postId: Long) {
        commentsLiveData =
            commentsRepository.getComments(postId).asLiveData(viewModelScope.coroutineContext)
    }

}