package com.tessafold.hellofolk.ui.posts

import android.app.ActivityOptions
import androidx.core.app.ActivityOptionsCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.tessafold.hellofolk.R
import com.tessafold.hellofolk.data.model.Post
import com.tessafold.hellofolk.data.network.Resource
import com.tessafold.hellofolk.databinding.ActivityPostsBinding
import com.tessafold.hellofolk.ui.base.BaseActivity
import com.tessafold.hellofolk.ui.post_details.PostDetailsActivity
import kotlinx.android.synthetic.main.activity_posts.*
import org.kodein.di.instance

class PostsActivity : BaseActivity<PostsViewModel, ActivityPostsBinding>(), PostsListener {

    override val layoutId: Int = R.layout.activity_posts

    override val viewModelFactory: ViewModelProvider.Factory by instance<PostsViewModelFactory>()

    override val viewModelClass: Class<PostsViewModel> = PostsViewModel::class.java

    override fun initUi() {
    }

    override fun readIntent() {
    }

    override fun getData() {
        viewModel.fetchData()
        viewModel.mediatorLiveData.observe(this, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    postsRecyclerView.adapter = PostsAdapter(it.data ?: emptyList(), this)
                }
                Resource.Status.ERROR -> {

                }
                Resource.Status.LOADING -> {
                }
            }
        })
    }

    override fun onItemClicked(post: Post) {
        val intent = PostDetailsActivity.getIntent(context = this, post = post)
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
    }

}