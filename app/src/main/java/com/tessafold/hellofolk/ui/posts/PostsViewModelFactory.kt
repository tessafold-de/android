package com.tessafold.hellofolk.ui.posts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tessafold.hellofolk.data.repository.PostsRepository
import com.tessafold.hellofolk.data.repository.UsersRepository

class PostsViewModelFactory(val postsRepository: PostsRepository, val usersRepository: UsersRepository) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostsViewModel(postsRepository, usersRepository) as T
    }

}