package com.tessafold.hellofolk.ui.intro

interface IntroListeners {

    fun onNextPage()

    fun onPreviousPage()

}