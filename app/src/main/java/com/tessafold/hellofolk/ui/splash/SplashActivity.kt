package com.tessafold.hellofolk.ui.splash

import android.animation.Animator
import android.content.Intent
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.tessafold.hellofolk.R
import com.tessafold.hellofolk.databinding.ActivitySplashBinding
import com.tessafold.hellofolk.ui.base.BaseActivity
import com.tessafold.hellofolk.ui.intro.IntroActivity
import com.tessafold.hellofolk.ui.posts.PostsActivity
import com.tessafold.hellofolk.utils.animateBackgroundColor
import com.tessafold.hellofolk.utils.setImageDrawableWithAnimation
import kotlinx.android.synthetic.main.activity_splash.*
import org.kodein.di.instance
import java.util.*


class SplashActivity : BaseActivity<SplashViewModel, ActivitySplashBinding>(),
    Animator.AnimatorListener {

    override val layoutId: Int = R.layout.activity_splash

    override val viewModelFactory: ViewModelProvider.Factory by instance<SplashViewModelFactory>()

    override val viewModelClass: Class<SplashViewModel> = SplashViewModel::class.java

    override fun initUi() {
        // Background Color Transition
        rootLayout.animateBackgroundColor(
            this,
            2000,
            R.color.orange,
            R.color.colorPrimary,
            R.color.colorAccent
        )

    }

    override fun readIntent() {
    }

    override fun getData() {
    }

    /**
     *
     * We do only care about the animation end in order to swap the logo
     *
     */
    override fun onAnimationStart(animation: Animator?) {
    }

    override fun onAnimationEnd(animation: Animator?) {
        // Animate background color back to white
        rootLayout.animateBackgroundColor(null, 500, R.color.white)

        // Animate logo to the colored logo
        imageView.setImageDrawableWithAnimation(
            ContextCompat.getDrawable(
                this,
                R.drawable.ic_logo_colored
            )!!
        )

        // Start the next activity 1 second later
        Timer().schedule(object : TimerTask() {
            override fun run() {
                val intent = if (viewModel.isFirstOpen()) {
                    Intent(this@SplashActivity, IntroActivity::class.java)
                } else {
                    Intent(this@SplashActivity, PostsActivity::class.java)
                }
                startActivity(intent)
                finish()
            }
        }, 1000)
    }

    override fun onAnimationCancel(animation: Animator?) {
    }

    override fun onAnimationRepeat(animation: Animator?) {
    }

}