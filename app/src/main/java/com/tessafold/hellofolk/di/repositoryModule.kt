package com.tessafold.hellofolk.di

import com.tessafold.hellofolk.data.repository.*
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.instance
import org.kodein.di.singleton

val repositoryModule = DI.Module(name = "Repository Module") {
    bind<PostsRepository>() with singleton {
        PostsRepositoryImpl(postDao = instance(), appServices = instance())
    }

    bind<UsersRepository>() with singleton {
        UserRepositoryImpl(userDao = instance(), appServices = instance())
    }

    bind<CommentsRepository>() with singleton {
        CommentsRepositoryImpl(commentDao = instance(), appServices = instance())
    }
}