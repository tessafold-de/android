package com.tessafold.hellofolk.di

import com.tessafold.hellofolk.data.network.retrofit.AppServices
import com.tessafold.hellofolk.data.network.retrofit.FlowCallAdapterFactory
import okhttp3.OkHttpClient
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.singleton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val networkModule = DI.Module(name = "Network Module") {

    val apiUrl = "http://jsonplaceholder.typicode.com/"

    val okHttpClient = OkHttpClient.Builder()
        .followRedirects(true)
        .followRedirects(true)
        .retryOnConnectionFailure(true)
        .build()

    val retrofit = Retrofit.Builder()
        .baseUrl(apiUrl)
        .client(okHttpClient)
        .addCallAdapterFactory(FlowCallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(AppServices::class.java)

    bind<AppServices>() with singleton { retrofit }

}