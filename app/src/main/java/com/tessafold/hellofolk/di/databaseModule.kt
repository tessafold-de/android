package com.tessafold.hellofolk.di

import com.tessafold.hellofolk.MyApplication
import com.tessafold.hellofolk.data.db.AppDatabase
import com.tessafold.hellofolk.data.db.dao.CommentDao
import com.tessafold.hellofolk.data.db.dao.PostDao
import com.tessafold.hellofolk.data.db.dao.UserDao
import com.tessafold.hellofolk.data.prefs.AppPreferences
import com.tessafold.hellofolk.data.prefs.AppPreferencesImpl
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.instance
import org.kodein.di.singleton

// This module injects Room Database & DAOs

val databaseModule = DI.Module(name = "Database Module") {
    bind<AppDatabase>() with singleton { AppDatabase.getDatabase(MyApplication.context) }
    bind<AppPreferences>() with singleton { AppPreferencesImpl(MyApplication.context) }

    bind<PostDao>() with singleton { instance<AppDatabase>().getPostDao() }
    bind<UserDao>() with singleton { instance<AppDatabase>().getUserDao() }
    bind<CommentDao>() with singleton { instance<AppDatabase>().getCommentDao() }
}