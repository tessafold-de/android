package com.tessafold.hellofolk.di

import com.tessafold.hellofolk.ui.intro.IntroViewModelFactory
import com.tessafold.hellofolk.ui.post_details.PostDetailsViewModel
import com.tessafold.hellofolk.ui.post_details.PostDetailsViewModelFactory
import com.tessafold.hellofolk.ui.posts.PostsViewModelFactory
import com.tessafold.hellofolk.ui.splash.SplashViewModelFactory
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.instance
import org.kodein.di.provider

val viewModelModule = DI.Module(name = "ViewModelModule") {
    bind<PostsViewModelFactory>() with provider {
        PostsViewModelFactory(
            postsRepository = instance(),
            usersRepository = instance()
        )
    }
    bind<SplashViewModelFactory>() with provider { SplashViewModelFactory(instance()) }
    bind<IntroViewModelFactory>() with provider { IntroViewModelFactory(instance()) }
    bind<PostDetailsViewModelFactory>() with provider { PostDetailsViewModelFactory(instance()) }
}