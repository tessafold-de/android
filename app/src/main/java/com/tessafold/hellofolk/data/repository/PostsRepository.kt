package com.tessafold.hellofolk.data.repository

import com.tessafold.hellofolk.data.db.dao.PostDao
import com.tessafold.hellofolk.data.model.Post
import com.tessafold.hellofolk.data.network.Resource
import com.tessafold.hellofolk.data.network.networkBoundResource
import com.tessafold.hellofolk.data.network.retrofit.AppServices
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

interface PostsRepository {

    fun getPosts(): Flow<Resource<List<Post>>>
}

class PostsRepositoryImpl(
    private val postDao: PostDao,
    private val appServices: AppServices
) :
    PostsRepository {

    override fun getPosts(): Flow<Resource<List<Post>>> {
        return networkBoundResource(
            fetchFromLocal = { postDao.getPosts() },
            shouldFetchFromRemote = { it == null || it.isEmpty() },
            fetchFromRemote = { appServices.getPosts() },
            processRemoteResponse = {},
            saveRemoteData = { postDao.delsert(it) },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }
}