package com.tessafold.hellofolk.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class User(
    @PrimaryKey
    val id: Long,
    val name: String,
    val email: String,
    val phone: String
): Serializable
