package com.tessafold.hellofolk.data.network.retrofit

import com.tessafold.hellofolk.data.model.Comment
import com.tessafold.hellofolk.data.model.Post
import com.tessafold.hellofolk.data.model.User
import com.tessafold.hellofolk.data.network.ApiResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET

interface AppServices {

    @GET("posts")
    fun getPosts(): Flow<ApiResponse<List<Post>>>

    @GET("users")
    fun getUsers(): Flow<ApiResponse<List<User>>>

    @GET("comments")
    fun getComments(): Flow<ApiResponse<List<Comment>>>

}