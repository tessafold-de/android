package com.tessafold.hellofolk.data.db.dao

import androidx.room.*
import com.tessafold.hellofolk.data.model.User
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {

    @Query("SELECT * FROM User")
    fun getUsers(): Flow<List<User>>

    @Query("SELECT * FROM User WHERE id = :id")
    fun getUserById(id: Long): Flow<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<User>)

    @Query("DELETE FROM User")
    fun deleteAll()

    @Transaction
    fun delsert(users: List<User>) {
        deleteAll()
        insertAll(users)
    }
}