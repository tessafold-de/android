package com.tessafold.hellofolk.data.db.dao

import androidx.room.*
import com.tessafold.hellofolk.data.model.Post
import kotlinx.coroutines.flow.Flow

@Dao
interface PostDao {

    @Query("SELECT * FROM Post")
    fun getPosts(): Flow<List<Post>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(posts: List<Post>)

    @Query("DELETE FROM Post")
    fun deleteAll()

    @Transaction
    fun delsert(posts: List<Post>) {
        deleteAll()
        insertAll(posts)
    }
}