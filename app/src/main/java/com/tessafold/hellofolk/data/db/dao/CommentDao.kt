package com.tessafold.hellofolk.data.db.dao

import androidx.room.*
import com.tessafold.hellofolk.data.model.Comment
import kotlinx.coroutines.flow.Flow

@Dao
interface CommentDao {

    @Query("SELECT * FROM Comment")
    fun getComments(): Flow<List<Comment>>

    @Query("SELECT * FROM Comment WHERE postId = :postId")
    fun getComments(postId: Long): Flow<List<Comment>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(comments: List<Comment>)

    @Query("DELETE FROM Comment")
    fun deleteAll()

    @Transaction
    fun delsert(comments: List<Comment>) {
        deleteAll()
        insertAll(comments)
    }
}