package com.tessafold.hellofolk.data.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Post(
    @PrimaryKey
    val id: Long,
    val userId: Long,
    val title: String,
    val body: String
) : Serializable {
    @Ignore
    var user: User? = null
}
