package com.tessafold.hellofolk.data.repository

import com.tessafold.hellofolk.data.db.dao.CommentDao
import com.tessafold.hellofolk.data.model.Comment
import com.tessafold.hellofolk.data.network.Resource
import com.tessafold.hellofolk.data.network.networkBoundResource
import com.tessafold.hellofolk.data.network.retrofit.AppServices
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

interface CommentsRepository {

    fun getComments(): Flow<Resource<List<Comment>>>

    fun getComments(postId: Long): Flow<Resource<List<Comment>>>

}

class CommentsRepositoryImpl(
    private val commentDao: CommentDao,
    private val appServices: AppServices

) : CommentsRepository {
    override fun getComments(): Flow<Resource<List<Comment>>> {
        return networkBoundResource(
            fetchFromLocal = { commentDao.getComments() },
            shouldFetchFromRemote = { comments -> comments.isNullOrEmpty() },
            fetchFromRemote = { appServices.getComments() },
            processRemoteResponse = {},
            saveRemoteData = { users -> commentDao.delsert(users) },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }

    override fun getComments(postId: Long): Flow<Resource<List<Comment>>> {
        return networkBoundResource(
            fetchFromLocal = { commentDao.getComments(postId) },
            shouldFetchFromRemote = { comments -> comments.isNullOrEmpty() },
            fetchFromRemote = { appServices.getComments() },
            processRemoteResponse = {},
            saveRemoteData = { users -> commentDao.delsert(users) },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }

}