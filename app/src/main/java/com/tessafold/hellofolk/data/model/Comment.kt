package com.tessafold.hellofolk.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Comment(
    @PrimaryKey
    val id: Long,
    val postId: Long,
    val name: String,
    val email: String,
    val body: String
)
