package com.tessafold.hellofolk.data.prefs

interface AppPreferences {

    fun isFirstOpen(): Boolean

    fun setFirstOpen(value: Boolean)

}