package com.tessafold.hellofolk.data.repository

import android.util.Log
import com.tessafold.hellofolk.data.db.dao.UserDao
import com.tessafold.hellofolk.data.model.User
import com.tessafold.hellofolk.data.network.Resource
import com.tessafold.hellofolk.data.network.networkBoundResource
import com.tessafold.hellofolk.data.network.retrofit.AppServices
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn

interface UsersRepository {

    fun getUsers(): Flow<Resource<List<User>>>

    fun getUser(userId: Long): Flow<Resource<User>>

}

class UserRepositoryImpl(
    private val userDao: UserDao,
    private val appServices: AppServices
) : UsersRepository {

    override fun getUsers(): Flow<Resource<List<User>>> {
        return networkBoundResource(
            fetchFromLocal = { userDao.getUsers() },
            shouldFetchFromRemote = { users -> users.isNullOrEmpty() },
            fetchFromRemote = { appServices.getUsers() },
            processRemoteResponse = {},
            saveRemoteData = { users -> userDao.delsert(users) },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }

    override fun getUser(userId: Long): Flow<Resource<User>> {
        return networkBoundResource(
            fetchFromLocal = { userDao.getUserById(userId) },
            shouldFetchFromRemote = { user -> user == null },
            fetchFromRemote = { appServices.getUsers() },
            processRemoteResponse = {},
            saveRemoteData = { users -> userDao.delsert(users) },
            onFetchFailed = { _, _ -> }
        ).flowOn(Dispatchers.IO)
    }

}