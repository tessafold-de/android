package com.tessafold.hellofolk.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.tessafold.hellofolk.data.db.dao.CommentDao
import com.tessafold.hellofolk.data.db.dao.PostDao
import com.tessafold.hellofolk.data.db.dao.UserDao
import com.tessafold.hellofolk.data.model.Comment
import com.tessafold.hellofolk.data.model.Post
import com.tessafold.hellofolk.data.model.User

const val DATABASE_NAME = "local_db";

@Database(
    entities = [
        Post::class,
        User::class,
        Comment::class
    ],
    version = 3
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getPostDao(): PostDao

    abstract fun getUserDao(): UserDao

    abstract fun getCommentDao(): CommentDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    DATABASE_NAME
                )
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                // return INSTANCE
                instance
            }
        }
    }
}