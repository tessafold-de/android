package com.tessafold.hellofolk.data.network

import kotlinx.coroutines.flow.*

inline fun <DB, REMOTE> networkBoundResource(
    crossinline fetchFromLocal: () -> Flow<DB>,
    crossinline shouldFetchFromRemote: (DB?) -> Boolean = { true },
    crossinline fetchFromRemote: () -> Flow<ApiResponse<REMOTE>>,
    crossinline processRemoteResponse: (response: ApiResponse.ApiSuccessResponse<REMOTE>) -> Unit = { },
    crossinline saveRemoteData: (REMOTE) -> Unit = { },
    crossinline onFetchFailed: (errorBody: String?, statusCode: Int) -> Unit = { _: String?, _: Int -> }
) = flow {
    emit(Resource.loading(data = null))

    val localData = fetchFromLocal().first()

    if (shouldFetchFromRemote(localData)) {
        emit(Resource.loading(data = localData))

        fetchFromRemote().collect { apiResponse ->
            when (apiResponse) {
                is ApiResponse.ApiSuccessResponse -> {
                    processRemoteResponse(apiResponse)
                    apiResponse.body?.let(saveRemoteData)
                    emitAll(fetchFromLocal().map { dbData -> Resource.success(data = dbData) })
                }

                is ApiResponse.ApiErrorResponse -> {
                    onFetchFailed(apiResponse.errorMessage, apiResponse.statusCode)
                    emitAll(fetchFromLocal().map { Resource.error(apiResponse.errorMessage, it) })
                }
            }
        }
    } else {
        emitAll(fetchFromLocal().map { Resource.success(it) })
    }
}

