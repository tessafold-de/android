package com.tessafold.hellofolk.data.prefs

import android.content.Context

private const val SHARED_PREFS_NAME = "HELLO_FOLK"
private const val FIRST_OPEN_KEY = "FIRST_OPEN"

class AppPreferencesImpl(context: Context) : AppPreferences {

    private val sharedPreferences =
        context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)


    override fun isFirstOpen(): Boolean = sharedPreferences.getBoolean(FIRST_OPEN_KEY, true)

    override fun setFirstOpen(value: Boolean) {
        sharedPreferences.edit().putBoolean(FIRST_OPEN_KEY, value).apply()
    }

}