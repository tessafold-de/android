package com.tessafold.hellofolk.data.network

import android.util.Log
import retrofit2.Response
import java.io.IOException
import java.net.SocketException
import java.net.SocketTimeoutException

/**
 *
 * Our API Client `Retrofit` will return one of these possible responses [Success, Empty, Error]
 *
 */
sealed class ApiResponse<T> {
    companion object {
        fun <T> create(error: Throwable): ApiErrorResponse<T> {
            val message: String? = if(error is IOException){
                "Check your internet connection and try again"
            }else{
                error.message
            }
            return ApiErrorResponse(
                errorMessage = message ?: "unknown error",
                statusCode = 0
            )
        }

        fun <T> create(response: Response<T>): ApiResponse<T> {
            return if (response.isSuccessful) {
                val body = response.body()
                return if (body == null || response.code() == 204) {
                    ApiEmptyResponse()
                } else {
                    ApiSuccessResponse(
                        body = response.body()
                    )
                }
            } else {
                val msg = response.errorBody()?.toString()
                val errorMessage = if (msg.isNullOrEmpty()) {
                    response.message()
                } else {
                    msg
                }
                ApiErrorResponse(
                    errorMessage = errorMessage,
                    statusCode = response.code()
                )
            }
        }
    }


    /*
    separate class for HTTP 204 responses so that we can make ApiSuccessResponse's body non-null.
    */
    class ApiEmptyResponse<T> : ApiResponse<T>()

    data class ApiSuccessResponse<T>(
        val body: T?
    ) : ApiResponse<T>()

    data class ApiErrorResponse<T>(
        val errorMessage: String,
        val statusCode: Int
    ) : ApiResponse<T>()
}
