package com.tessafold.hellofolk.utils

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.TransitionDrawable
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import androidx.core.content.ContextCompat

fun Context.getColorInt(colorId: Int): Int {
    return ContextCompat.getColor(this, colorId)
}

fun ImageView.setImageDrawableWithAnimation(drawable: Drawable, duration: Int = 500) {
    val currentDrawable = getDrawable()
    if (currentDrawable == null) {
        setImageDrawable(drawable)
        return
    }

    val transitionDrawable = TransitionDrawable(
        arrayOf(
            currentDrawable,
            drawable
        )
    )
    setImageDrawable(transitionDrawable)
    transitionDrawable.isCrossFadeEnabled = true
    transitionDrawable.startTransition(duration)
}

@SuppressLint("ObjectAnimatorBinding")
fun View.animateBackgroundColor(
    listener: Animator.AnimatorListener?,
    duration: Long = 2000,
    vararg colors: Int
) {
    val objectAnimator = ObjectAnimator.ofInt(
        this.background!!,
        "color",
        *colors.map { context.getColorInt(it) }.toIntArray()
    )

    objectAnimator.interpolator = LinearInterpolator()
    objectAnimator.duration = duration
    objectAnimator.setEvaluator(ArgbEvaluator())
    val t = AnimatorSet()
    t.play(objectAnimator)
    listener?.let { t.addListener(listener) }
    t.start()
}