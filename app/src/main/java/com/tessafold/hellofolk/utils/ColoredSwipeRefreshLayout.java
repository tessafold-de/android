package com.tessafold.hellofolk.utils;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.tessafold.hellofolk.R;

public class ColoredSwipeRefreshLayout extends SwipeRefreshLayout {

    public ColoredSwipeRefreshLayout(@NonNull Context context) {
        super(context);
        setColors();
    }

    public ColoredSwipeRefreshLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setColors();
    }

    private void setColors() {
        setColorSchemeColors(
                ContextCompat.getColor(getContext(), R.color.colorPrimary),
                ContextCompat.getColor(getContext(), R.color.colorPrimaryDark),
                ContextCompat.getColor(getContext(), R.color.colorAccent));
    }
}