package com.tessafold.hellofolk

import android.app.Application
import android.content.Context
import com.tessafold.hellofolk.di.databaseModule
import com.tessafold.hellofolk.di.networkModule
import com.tessafold.hellofolk.di.repositoryModule
import com.tessafold.hellofolk.di.viewModelModule
import org.kodein.di.DI
import org.kodein.di.DIAware


class MyApplication : Application(), DIAware {

    companion object {
        private lateinit var sApplication: Application
        val context: Context
            get() = sApplication.applicationContext
    }


    override val di by DI.lazy {

        // Retrofit Client & AppServices Interface
        import(networkModule)

        // Room Database & DAOs
        import(databaseModule)

        // Repositories
        import(repositoryModule)

        // ViewModels
        import(viewModelModule)

    }

    override fun onCreate() {
        super.onCreate()
        sApplication = this
    }


}