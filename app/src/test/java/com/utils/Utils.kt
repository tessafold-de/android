package com.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.tessafold.hellofolk.data.model.Post
import com.tessafold.hellofolk.data.model.User
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

fun generatePosts(): List<Post> {
    val result: MutableList<Post> = mutableListOf()
    result += Post(id = 1, userId = 1, title = "Post Title", body = "Post Body")
    result += Post(id = 2, userId = 2, title = "Post Title 2", body = "Post Body 2")
    return result
}

fun generateUsers(): List<User> {
    val result: MutableList<User> = mutableListOf()
    result += User(
        id = 1,
        name = "Hamza Al Omari",
        email = "omarihamza@outlook.com",
        phone = "+41132456789"
    )
    result += User(
        id = 2,
        name = "User",
        email = "user@mail.de",
        phone = "+41132456789"
    )
    return result
}

fun <T> LiveData<T>.getOrAwaitValue(
    time: Long = 2,
    timeUnit: TimeUnit = TimeUnit.SECONDS
): T {
    var data: T? = null
    val latch = CountDownLatch(1)
    val observer = object : Observer<T> {
        override fun onChanged(o: T?) {
            data = o
            latch.countDown()
            this@getOrAwaitValue.removeObserver(this)
        }
    }

    this.observeForever(observer)

    // Don't wait indefinitely if the LiveData is not set.
    if (!latch.await(time, timeUnit)) {
        throw TimeoutException("LiveData value was never set.")
    }

    @Suppress("UNCHECKED_CAST")
    return data as T
}