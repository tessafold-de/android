package com.tessafold.hellofolk.ui.posts

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.tessafold.hellofolk.data.model.Post
import com.tessafold.hellofolk.data.network.Resource
import com.tessafold.hellofolk.data.repository.PostsRepository
import com.tessafold.hellofolk.data.repository.UsersRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule


/**
 * In this class we're going to provide an example of Unit Testing
 */

@ExperimentalCoroutinesApi
class PostsViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val userRepository = mockk<UsersRepository>()
    private val postsRepository = mockk<PostsRepository>()

    private val viewModel = PostsViewModel(postsRepository, userRepository)

    lateinit var observer: Observer<Resource<List<Post>>>

    /**
     * -- Expected Flow --
     *
     * We need to chain 2 API requests in order to fetch the author (aka. user) of the post
     * That's why we need to user a mediator live data
     * 1 -> we fetch the posts
     * 2 -> when the posts are fetched successfully we need to fetch the users and map them to the posts
     *
     */

    @Before
    fun setup() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        viewModel.mediatorLiveData.observeForever { }
    }

    @After
    fun teardown() {
        Dispatchers.resetMain()
    }

    /**
     *
     *      fetchData() method should add postsRepo.getPosts() data as a source in the MediatorLiveData
     *      MediatorLiveData should react as follows:
     *             Status Error, Loading -> MediatorLiveData.Status = Error, Loading
     *             Status Success -> fetchUsers()
     *
     */

    @Test
    fun mediatorLiveDataStatusShouldBeLoadingWhenPostsStatusIsLoading() {
        // Arrange
        every { postsRepository.getPosts() } returns flowOf(Resource.loading(data = null))

        // Act
        viewModel.fetchData()

        // Assert
        verify { postsRepository.getPosts() }
        verify(exactly = 0) { userRepository.getUsers() }
        assertEquals(viewModel.mediatorLiveData.value?.status, Resource.Status.LOADING)
    }

    @Test
    fun mediatorLiveDataStatusShouldBeErrorWhenPostsStatusIsError() {
        // Arrange
        val errorMessage = "an error has occurred."
        every { postsRepository.getPosts() } returns flowOf(Resource.error(errorMessage))

        // Act
        viewModel.fetchData()

        // Assert
        verify { postsRepository.getPosts() }
        verify(exactly = 0) { userRepository.getUsers() }
        assertEquals(viewModel.mediatorLiveData.value?.status, Resource.Status.ERROR)
        assertEquals(viewModel.mediatorLiveData.value?.message, errorMessage)
    }

    /**
     * AND SO ON! :)
     */


}